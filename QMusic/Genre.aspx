﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Genre.aspx.cs" Inherits="QMusic.Genre" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><small>List</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Genre</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="GenreList">
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add Genre </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br>
                <form id=" AddGenre" class="form-horizontal form-label-left" novalidate="">

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="hidden" id="GenreId" value="-1" required="required" class="form-control col-md-7 col-xs-12" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="GenreName">
                            Genre Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="GenreName" maxlength="50" name="last-name" required="required" class="form-control col-md-7 col-xs-12" />
                        </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <input type="button" onclick="ClearFields()" class="btn btn-primary" value="Cancel" />
                            <input type="button" onclick="AddRecord()" class="btn btn-success" value="Add" />
                        </div>
                    </div>

                </form>
                <div class="col-lg-12">
                    <div id="Result" class="alert alert-dismissible fade in" role="alert">
                        <strong id="ResultMsg"></strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
     <script src="/Scripts/main.js"></script>
</asp:Content>
