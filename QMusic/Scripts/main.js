
$(document).ready(function(){
  
 GetGenre();
 EnableControls();
 ClearFields();
$('#GenreId').val(-1)
});

function EnableControls(){
	$('#Result').hide();
}

function ajaxHelper(url, method, data) {

    return $.ajax({
            type: method,
            url: url,
            dataType: 'json',
            contentType: 'application/json',
            data: data != "" ? JSON.stringify(data) : null
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
					$('#Result').show();
				    $('#Result').removeClass('alert-success').addClass('alert-danger');
					$('#ResultMsg').html('Sorry! unable to save:' + errorThrown);
        })
        .always(function() {

        });
}

function ClearFields(){
	$('#GenreId').val(-1);
	 $('#GenreName').val("");
	 	$('#Result').hide();
		    $('#Result').addClass('alert-danger');
		    $('#ResultMsg').html('');	
}

function GetGenre() {
	$('#GenreList').html('');
    ajaxHelper('http://www.qmusic.trillbay.com/api/Genres', 'GET', '').done(function(data) {
        var _genre = data; 
	    var tr;
		for (var i = 0; i < _genre.length; i++) {
					//$('#GenrelistLink').append("<li><a href=''>" + _genre[i].GenreName + "</a></li>");
		    tr = $('<tr/>');
            tr.append("<td>" + _genre[i].GenreName + "</td>");
			tr.append("<td>" + "<a class='btn' onclick='editGenre(" + _genre[i].GenreId + ")'><i class='fa fa-edit'></i></a>" + "<a class='btn' onclick='deletGenre(" + _genre[i].GenreId + ")'><i class='fa fa-trash'></i></a>" + "</td>");
            $('#GenreList').append(tr);

		}
	});
}

function AddRecord(){
	
	if ($('#GenreId').val()== -1){
       AddGenre();
	}
	else{
		UpdateGenre($('#GenreId').val());
	}

	GetGenre();
}


function deletGenre(id){
 ajaxHelper('http://www.qmusic.trillbay.com/api/Genres/' + id, 'DELETE', '').done(function(data) {
         	$('#Result').show();
			$('#Result').removeClass('alert-danger').addClass('alert-success');
		    $('#ResultMsg').html('Deleted successfully!');	
			GetGenre();	
	});
}

function UpdateGenre(id) {

	         var _data = new Object();
			_data.GenreId =	$('#GenreId').val();
			_data.GenreName = $('#GenreName').val();


    ajaxHelper('http://www.qmusic.trillbay.com/api/Genres/' + id, 'PUT', _data).done(function(data) {
      	$('#Result').show();
		$('#Result').removeClass('alert-danger').addClass('alert-success');
		$('#ResultMsg').html('Updated successfully!');	
	}); 

}


function editGenre(id) {
    ajaxHelper('http://www.qmusic.trillbay.com/api/Genres/' + id, 'GET', '').done(function(data) {
        var _genre = data; 
		$('#GenreId').val(_genre.GenreId);
		$('#GenreName').val(_genre.GenreName);
	});
}

function AddGenre() {

			var _data = new Object();
			_data.GenreId =	$('#GenreId').val();
			_data.GenreName = $('#GenreName').val();

			ajaxHelper('http://qmusic.trillbay.com/api/Genres', 'POST', _data).done(function(data) {
			$('#Result').show();
			$('#Result').removeClass('alert-danger').addClass('alert-success');
		    $('#ResultMsg').html('Genre saved successfully');			
      
    });
}
